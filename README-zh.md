# 安装依赖
使用 npm i 下载依赖

# 启动命令
使用 npm run serve 命令启动

# 结论
1. weex和weex-ui文档写得不是很好，遇到问题去网上找答案发现大部分答案都集中在2018年，18年至今的问题和答案很少，社区活跃度不高，遇到问题不容易找到答案。

2. 跨平台能力上，一些样式在ios，安卓，web上表现不一致，总是需要不停的调整。

# 截图和说明
[https://ayou1110.blog.csdn.net/article/details/114199552](https://ayou1110.blog.csdn.net/article/details/114199552)