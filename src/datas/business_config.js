export default {

    tabTitles: [
      {
        title: '附近推荐',
        // icon: 'https://gw.alicdn.com/tfs/TB1MWXdSpXXXXcmXXXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1kCk2SXXXXXXFXFXXXXXXXXXX-72-72.png',
      },
      {
        title: '发现好菜',
        // icon: 'https://gw.alicdn.com/tfs/TB1ARoKSXXXXXc9XVXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB19Z72SXXXXXamXFXXXXXXXXXX-72-72.png'
      },
      {
        title: '百货',
        // icon: 'https://gw.alicdn.com/tfs/TB1VKMISXXXXXbyaXXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1aTgZSXXXXXazXFXXXXXXXXXX-72-72.png'
      },
      {
        title: '超市',
        // icon: 'https://gw.alicdn.com/tfs/TB1Do3tSXXXXXXMaFXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1LiNhSpXXXXaWXXXXXXXXXXXX-72-72.png'
      },
      {
        title: '水果',
        // icon: 'https://gw.alicdn.com/tfs/TB1jFsLSXXXXXX_aXXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1_Kc.SXXXXXa8XpXXXXXXXXXX-72-72.png'
      },
      {
        title: '买菜',
        // icon: 'https://gw.alicdn.com/tfs/TB199sPSXXXXXb4XVXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1DR.3SXXXXXc2XpXXXXXXXXXX-72-72.png'
      }, {
        title: '医药',
        // icon: 'https://gw.alicdn.com/tfs/TB1hedfSpXXXXchXXXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1mrXaSpXXXXaqXpXXXXXXXXXX-72-72.png'
      },
      {
        title: '零售',
        // icon: 'https://gw.alicdn.com/tfs/TB1twhkSpXXXXXLXXXXXXXXXXXX-72-72.png',
        // activeIcon: 'https://gw.alicdn.com/tfs/TB1dhlhSpXXXXa8XXXXXXXXXXXX-72-72.png'
      }
    ],
    tabStyles: {
      bgColor: '#FFFFFF',
      titleColor: '#666666',
      activeTitleColor: '#3D3D3D',
      activeBgColor: '#FFFFFF',
      isActiveTitleBold: true,
      iconWidth: 70,
      iconHeight: 10,
      width: 130,
      height: 100,
      fontSize: 24,
      hasActiveBottom: true,
      activeBottomColor: '#1E90FF',
      activeBottomHeight: 5,
      activeBottomWidth: 60,
      textPaddingLeft: 10,
      textPaddingRight: 10,
      normalBottomColor: 'rgba(0,0,0,0)',
      normalBottomHeight: 2,
      hasRightIcon: true,
      rightOffset: 100
    },
    // 使用 iconfont 模式的tab title配置
    tabIconFontTitles: [
      {
        title: '首页',
        codePoint: '\ue623'
      },
      {
        title: '特别推荐',
        codePoint: '\ue608'
      },
      {
        title: '消息中心',
        codePoint: '\ue752',
        badge: 5
      },
      {
        title: '我的主页',
        codePoint: '\ue601',
        dot: true
      }
    ],
    tabIconFontStyles: {
      bgColor: '#FFFFFF',
      titleColor: '#666666',
      activeTitleColor: '#3D3D3D',
      activeBgColor: '#FFFFFF',
      isActiveTitleBold: true,
      width: 160,
      height: 120,
      fontSize: 24,
      textPaddingLeft: 10,
      textPaddingRight: 10,
      iconFontSize: 50,
      iconFontColor: '#333333',
      iconFontMarginBottom: 8,
      activeIconFontColor: 'red',
      iconFontUrl: '//at.alicdn.com/t/font_501019_mauqv15evc1pp66r.ttf'
    },
    BusinessInformation: [
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ],
      [
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(市中心店)',
          score: '4.8',
          monthSale: '1789',
          deliveryTime: '30分钟',
          distance: '6km',
          standard: '¥20',
          distributionFee: '¥8',
          evaluate: '味道很不错！味道很不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(南太湖店)',
          score: '4.9',
          monthSale: '2000',
          deliveryTime: '40分钟',
          distance: '8km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '鸡块不错！鸡块不错！'
        },
        {
          icon: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3995323738,3528253073&fm=26&gp=0.jpg',
          title: '肯德基宅急送(学院路店)',
          score: '4.8',
          monthSale: '2123',
          deliveryTime: '45分钟',
          distance: '10km',
          standard: '¥20',
          distributionFee: '¥10',
          evaluate: '蛋挞好吃！蛋挞好吃！'
        }
      ]
    ],
    advertisementImg:[
      "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgtao.fenlei265.com%2Fbao%2Fuploaded%2Fi2%2FO1CN012YBWAq1XjiuGvMsiT_%21%210-fleamarket.jpg_728x728.jpg&refer=http%3A%2F%2Fimgtao.fenlei265.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616594523&t=4ed2627565aed8be74b5aa29634235a4",
      "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2801675575,334357371&fm=26&gp=0.jpg",
      "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1542475394,3164900909&fm=26&gp=0.jpg",
      "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg9.doubanio.com%2Fview%2Fgroup_topic%2Fl%2Fpublic%2Fp186318094.jpg&refer=http%3A%2F%2Fimg9.doubanio.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616595441&t=6c9ea1760722128e23c80fb9a0b12960"
    ],
    menuDetails: [
      {
        menu_id: 1,
        menu_name: '美味汉堡/卷',
        lists:[
          {
            id: 1,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 2,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 3,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      },
      {
        menu_id: 2,
        menu_name: '串串/卤味',
        lists:[
          {
            id: 4,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 5,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 6,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      },
      {
        menu_id: 3,
        menu_name: '炸鸡/啤酒',
        lists:[
          {
            id: 7,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 8,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 9,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      },
      {
        menu_id: 4,
        menu_name: '原味鸡/脆皮鸡',
        lists:[
          {
            id: 10,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 11,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 12,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      },
      {
        menu_id: 5,
        menu_name: '鸡翅/鸡排',
        lists:[
          {
            id: 13,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 14,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 15,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      },
      {
        menu_id: 6,
        menu_name: '甜品/冰淇淋',
        lists:[
          {
            id: 16,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 17,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 18,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      },
      {
        menu_id: 7,
        menu_name: '缤纷·饮料',
        lists:[
          {
            id: 19,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥139',
            originalPrice: '208.5',
            num:0
          },
          {
            id: 20,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '110',
            presentPrice: '¥109',
            originalPrice: '178.5',
            num:0
          },
          {
            id: 21,
            icon: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F12%2F15%2F9a136d9c49367685561b42fb78e31de1.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616853613&t=113f162b9f8499defa3659fc412f9619',
            title: '超人气外带全家桶',
            details: '汉堡2个+鸡翅10块+原味鸡2块+新奥尔良烤翅2块+可乐3杯+玉米棒1个',
            monthSale: '100',
            presentPrice: '¥100',
            originalPrice: '108.5',
            num:0
          }
        ]
      }
    ]
  }
