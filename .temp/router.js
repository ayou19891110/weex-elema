import Vue from 'vue'
/*global Vue*/
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Business from '@/page/business'
import Menu from '@/page/menu'
import Order from '@/page/order'
import Introduce from '@/page/introduce'
import Evaluate from '@/page/evaluate'

Vue.use(Router)

export const router = new Router({
  routes: [
    { path: '/', redirect: '/business' },
    {
      path: '/business',
      name: 'Business',
      component: Business
    },
    {
      path: '/menu',
      name: 'Menu',
      component: Menu,
      children:[
        {
          path: '/order',
          name: 'Order',
          component: Order
        },
        {
          path: '/introduce',
          name: 'Introduce',
          component: Introduce
        },
        {
          path: '/evaluate',
          name: 'Evaluate',
          component: Evaluate
        },
      ]
    },
    // {
    //   path: '/order',
    //   name: 'Order',
    //   component: Order
    // },
    {
      path: '/hell',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})
